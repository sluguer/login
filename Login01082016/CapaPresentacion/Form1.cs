﻿using CapaLogica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void txtUsuario_TextChanged(object sender, EventArgs e)
        {

        }
        Login oLogin = new Login();
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            String retorno = oLogin.Muestra_Usuario(txtUsuario.Text,txtClave.Text);
            switch (retorno)
            {

                case "Administrador":
                    FrmAdministrador oAdministrador = new FrmAdministrador();
                    oAdministrador.Show();

                    this.Hide();
                    break;

                case "Medico":
                    FrmMedico oMedico = new FrmMedico();
                    oMedico.Show();

                    this.Hide();
                    break;

                case "Paciente":
                    FrmPaciente oPaciente = new FrmPaciente();
                    oPaciente.Show();

                    this.Hide();
                    break;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
           this.Close();
            //Application.Exit();
        }
    }
}
